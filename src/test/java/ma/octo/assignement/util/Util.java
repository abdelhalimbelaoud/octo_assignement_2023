package ma.octo.assignement.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {

    public static String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public static <T> T asJavaObject(String responseContent, Class<T> tClass) {
        try {
            return new ObjectMapper().readValue(responseContent, tClass);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

}
