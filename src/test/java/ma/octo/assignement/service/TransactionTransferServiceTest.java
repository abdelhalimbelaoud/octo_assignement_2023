package ma.octo.assignement.service;

import static ma.octo.assignement.constants.Constants.*;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.transaction.TransactionTransfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionTransferRepository;
import ma.octo.assignement.service.impl.AuditServiceImpl;
import ma.octo.assignement.service.impl.TransactionTransferServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TransactionTransferServiceTest {

    @InjectMocks
    TransactionTransferServiceImpl transferService;

    @Mock
    TransactionTransferRepository transferRepository;

    @Mock
    CompteRepository compteRepository;

    @Mock
    AuditRepository auditRepository;

    @Mock
    AuditServiceImpl auditService;

    private Compte compteBeneficiaire;

    private Compte compteEmetteur;

    private TransferDto transferDto;

    /*
        Method naming convention
        ------------------------
        [name_of_method]_[conditions]_[result]
    */

    @BeforeEach
    public void init() {
        compteEmetteur = Compte.builder().nrCompte("nrEmetteur")
                                         .rib("ribEmetteur")
                                         .solde(new BigDecimal(2000))
                                         .build();

        compteBeneficiaire = Compte.builder().nrCompte("nrBeneficiaire")
                                             .rib("ribBeneficiaire")
                                             .solde(new BigDecimal(1000))
                                             .build();

        transferDto = TransferDto.builder().nrCompteEmetteur(compteEmetteur.getNrCompte())
                                           .nrCompteBeneficiaire(compteBeneficiaire.getNrCompte())
                                           .motif("charge")
                                           .montant(new BigDecimal(500))
                                           .build();
    }

    @Test
    public void allTransferts_should_succeed() {
        List<TransactionTransfer> transferts = new ArrayList<>(){{
            add(TransactionTransfer.builder().compteEmetteur(new Compte())
                                             .compteBeneficiaire(new Compte())
                                             .build());
            add(TransactionTransfer.builder().compteEmetteur(new Compte())
                                             .compteBeneficiaire(new Compte())
                                             .build());
        }};

        List<TransferDto> transfertDtos = transferts.stream()
                                                    .map(TransferMapper::map)
                                                    .collect(Collectors.toList());

        given(transferRepository.findAll()).willReturn(transferts);

        assertEquals(transfertDtos, transferService.getAllTransferts());

        verify(transferRepository).findAll();
    }

    @Test
    public void transfer_should_succeed() {
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);

        //On assure que la méthode transfer s'éxecute sans lancer des exceptions
        assertDoesNotThrow(() -> transferService.transfer(transferDto));

        //On assure que le montant était bien transféré
        assertEquals(compteEmetteur.getSolde(), new BigDecimal(1500));
        assertEquals(compteBeneficiaire.getSolde(), new BigDecimal(1500));

        verify(compteRepository).save(compteEmetteur);
        verify(compteRepository).save(compteBeneficiaire);
        verify(transferRepository).save(any());
    }

    @Test
    public void transfer_with_compte_beneficiaire_non_existant_should_succeed() {
        transferDto.setNrCompteBeneficiaire("xxxNot_Existing_Accountxxx");
        Exception exception = assertThrows(CompteNonExistantException.class, () -> transferService.transfer(transferDto));
        assertEquals(COMPTE_NON_EXISTANT, exception.getMessage());
    }

    @Test
    public void transfer_with_compte_emetteur_non_existant_should_succeed() {
        transferDto.setNrCompteEmetteur("xxxNot_Existing_Accountxxx");
        Exception exception = assertThrows(CompteNonExistantException.class, () -> transferService.transfer(transferDto));
        assertEquals(COMPTE_NON_EXISTANT, exception.getMessage());
    }

    @Test
    public void transfer_with_min_amount_constraint_violation_should_fail() {
        transferDto.setMontant(BigDecimal.ONE);

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(MIN_AMOUNT_CONSTRAINT, exception.getMessage());
    }

    @Test
    public void transfer_with_max_amount_constraint_violation_should_fail() {
        transferDto.setMontant(BigDecimal.valueOf(Long.MAX_VALUE));

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(MAX_AMOUNT_CONSTRAINT, exception.getMessage());
    }

    @Test
    public void transfer_with_montant_null_should_fail() {
        transferDto.setMontant(null);

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(COMPTE_VIDE, exception.getMessage());
    }

    @Test
    public void transfer_with_montant_zero_should_fail() {
        transferDto.setMontant(BigDecimal.ZERO);

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(COMPTE_VIDE, exception.getMessage());
    }

    @Test
    public void transfer_with_motif_vide_should_fail() {
        transferDto.setMotif(null);

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(MOTIF_VIDE, exception.getMessage());
    }

    @Test
    public void transfer_with_blank_motif_should_fail() {
        transferDto.setMotif("   ");

        given(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).willReturn(compteBeneficiaire);
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);

        Exception exception = assertThrows(TransactionException.class, () -> transferService.transfer(transferDto));
        assertEquals(MOTIF_VIDE, exception.getMessage());
    }

    @AfterEach
    public void teardown() {
        compteEmetteur = null;
        compteBeneficiaire = null;
        transferDto = null;
    }

}
