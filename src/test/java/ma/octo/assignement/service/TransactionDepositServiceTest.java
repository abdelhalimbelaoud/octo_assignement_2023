package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.transaction.TransactionDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionDepositRepository;
import ma.octo.assignement.service.impl.TransactionDepositServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ma.octo.assignement.constants.Constants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TransactionDepositServiceTest {

    @InjectMocks
    private TransactionDepositServiceImpl depositService;

    @Mock
    private TransactionDepositRepository depositRepository;

    @Mock
    private CompteRepository compteRepository;

    @Mock
    private AuditRepository auditRepository;

    @Mock
    private IAuditService auditService;

    private Compte compteBeneficiaire;

    private DepositDto depositDto;

    /*
        Method naming convention
        ------------------------
        [name_of_method]_[conditions]_[result]
    */

    @BeforeEach
    public void init() {
        compteBeneficiaire = Compte.builder().nrCompte("nrBeneficiaire")
                                             .rib("ribBeneficiaire")
                                             .solde(new BigDecimal(2000))
                                             .build();

        depositDto = DepositDto.builder().nomEmetteur("Deposer")
                                         .rib("ribBeneficiaire")
                                         .montant(new BigDecimal(500))
                                         .motif("charge")
                                         .build();
    }

    @Test
    public void allDeposits_should_succeed() {
        List<TransactionDeposit> deposits = new ArrayList<>(){{
            add(new TransactionDeposit());
            add(new TransactionDeposit());
        }};

        List<DepositDto> depositDtos = deposits.stream()
                                               .map(DepositMapper::map)
                                               .collect(Collectors.toList());

        given(depositRepository.findAll()).willReturn(deposits);

        assertEquals(depositDtos, depositService.getAllDeposits());

        verify(depositRepository).findAll();
    }

    @Test
    public void deposit_should_succeed() {
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        assertDoesNotThrow(() -> depositService.deposit(depositDto));
        assertEquals(compteBeneficiaire.getSolde(), new BigDecimal(2500));

        verify(compteRepository).save(compteBeneficiaire);
        verify(depositRepository).save(any());
    }

    @Test
    public void deposit_with_compte_beneficiaire_non_existant_should_fail() {
        depositDto.setRib("xxxRandomRibxxx");
        Exception exception = assertThrows(CompteNonExistantException.class, () -> depositService.deposit(depositDto));
        assertEquals(COMPTE_NON_EXISTANT, exception.getMessage());
    }

    @Test
    public void deposit_with_min_amount_constraint_violation_should_fail() {
        depositDto.setMontant(BigDecimal.ONE);
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(MIN_AMOUNT_CONSTRAINT, exception.getMessage());
    }

    @Test
    public void deposit_with_max_amount_constraint_violation_should_fail() {
        depositDto.setMontant(BigDecimal.valueOf(Long.MAX_VALUE));
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(MAX_AMOUNT_CONSTRAINT, exception.getMessage());
    }

    @Test
    public void deposit_with_montant_null_should_fail() {
        depositDto.setMontant(null);
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(COMPTE_VIDE, exception.getMessage());
    }

    @Test
    public void deposit_with_montant_zero_should_fail() {
        depositDto.setMontant(BigDecimal.ZERO);
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(COMPTE_VIDE, exception.getMessage());
    }

    @Test
    public void deposit_with_motif_vide_should_fail() {
        depositDto.setMotif(null);
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(MOTIF_VIDE, exception.getMessage());
    }

    @Test
    public void deposit_with_blank_motif_should_fail() {
        depositDto.setMotif("   ");
        given(compteRepository.findByRib(compteBeneficiaire.getRib())).willReturn(compteBeneficiaire);

        Exception exception = assertThrows(TransactionException.class, () -> depositService.deposit(depositDto));
        assertEquals(MOTIF_VIDE, exception.getMessage());
    }

    @AfterEach
    public void teardown() {
        compteBeneficiaire = null;
        depositDto = null;
    }

}
