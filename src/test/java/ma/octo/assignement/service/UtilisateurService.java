package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.impl.UtilisateurServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UtilisateurService {

    @InjectMocks
    private UtilisateurServiceImpl utilisateurService;

    @Mock
    private UtilisateurRepository utilisateurRepository;

    @Test
    void allUtilisateur_should_succeed() {
        List<Utilisateur> utilisateurs = new ArrayList<>(){{
            add(Utilisateur.builder().nomUtilisateur("user1").build());
            add(Utilisateur.builder().nomUtilisateur("user2").build());
        }};

        List<UtilisateurDto> utilisateurDtos = utilisateurs.stream()
                                                           .map(UtilisateurMapper::map)
                                                           .collect(Collectors.toList());

        given(utilisateurRepository.findAll()).willReturn(utilisateurs);

        assertEquals(utilisateurDtos, utilisateurService.getAll());

        verify(utilisateurRepository).findAll();
    }

}
