package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.impl.CompteServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class compteServiceTest {

    @InjectMocks
    private CompteServiceImpl compteService;

    @Mock
    private CompteRepository compteRepository;

    @Test
    void allComptes_should_succeed() {
        List<Compte> comptes = new ArrayList<>(){{
            add(new Compte());
            add(new Compte());
        }};

        List<CompteDto> compteDtos = comptes.stream()
                                            .map(CompteMapper::map)
                                            .collect(Collectors.toList());

        given(compteRepository.findAll()).willReturn(comptes);

        assertEquals(compteDtos, compteService.getAll());

        verify(compteRepository).findAll();
    }

}
