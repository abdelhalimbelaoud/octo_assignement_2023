package ma.octo.assignement.controller;

import static ma.octo.assignement.constants.Constants.*;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static ma.octo.assignement.util.Util.asJsonString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private TransferDto transfertDto;

    final String ALL_TRANSFERT_PATH = "/api/transaction/transfert/tousLesTransferts";
    
    final String EFFECTUER_TRANSFERT_PATH = "/api/transaction/transfert/effectuerTransfert";

    @BeforeEach
    public void init() {
        transfertDto = TransferDto.builder().nrCompteEmetteur("000001")
                                            .nrCompteBeneficiaire("000002")
                                            .motif("charge")
                                            .montant(new BigDecimal(100))
                                            .build();
    }

    @Test
    public void allTransferts_should_succeed() throws Exception {
        mockMvc.perform(get(ALL_TRANSFERT_PATH).accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    public void effectuerTransfert_should_succeed() throws Exception {
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isCreated());
    }

    @Test
    public void effectuerTransfert_with_compte_emetteur_non_existant_should_fail() throws Exception {
        transfertDto.setNrCompteEmetteur("xxxRandomxxx");
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isNotFound(),
                       result -> assertTrue(result.getResolvedException() instanceof CompteNonExistantException),
                       result -> assertEquals(COMPTE_NON_EXISTANT, result.getResponse().getContentAsString())
               );
    }


    @Test
    public void effectuerTransfert_with_compte_beneficiaire_non_existant_should_fail() throws Exception {
        transfertDto.setNrCompteBeneficiaire("xxxRandomxxx");
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isNotFound(),
                       result -> assertTrue(result.getResolvedException() instanceof CompteNonExistantException),
                       result -> assertEquals(COMPTE_NON_EXISTANT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_min_amount_constraint_violation_should_fail() throws Exception {
        transfertDto.setMontant(BigDecimal.ONE);
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MIN_AMOUNT_CONSTRAINT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_max_amount_constraint_violation_should_fail() throws Exception {
        transfertDto.setMontant(new BigDecimal(10001));
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MAX_AMOUNT_CONSTRAINT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_montant_null_should_fail() throws Exception {
        transfertDto.setMontant(null);
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(COMPTE_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_montant_zero_should_fail() throws Exception {
        transfertDto.setMontant(BigDecimal.ZERO);
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(COMPTE_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_null_motif_should_fail() throws Exception {
        transfertDto.setMotif(null);
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MOTIF_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_blank_motif_should_fail() throws Exception {
        transfertDto.setMotif("       ");
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MOTIF_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerTransfert_with_solde_insuffisant_should_fail() throws Exception {
        transfertDto.setMontant(MAX_TRANSACTION);
        mockMvc.perform(post(EFFECTUER_TRANSFERT_PATH).content(asJsonString(transfertDto))
                                                      .contentType(MediaType.APPLICATION_JSON)
                                                      .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnavailableForLegalReasons(),
                       result -> assertTrue(result.getResolvedException() instanceof SoldeDisponibleInsuffisantException),
                       result -> assertEquals(SOLDE_INSUFFISANT, result.getResponse().getContentAsString())
               );
    }

    @AfterEach
    public void teardown() {
        transfertDto = null;
    }

}
