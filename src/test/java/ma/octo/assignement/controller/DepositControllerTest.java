package ma.octo.assignement.controller;

import static ma.octo.assignement.constants.Constants.*;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static ma.octo.assignement.util.Util.asJsonString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class DepositControllerTest {

    @Autowired
    MockMvc mockMvc;

    private DepositDto depositDto;

    final String ALL_DEPOSIT_PATH = "/api/transaction/deposit/tousLesDeposits";
    final String EFFECTUER_DEPOSIT_PATH = "/api/transaction/deposit/effectuerDeposit";

    @BeforeEach
    public void init() {
        depositDto = DepositDto.builder().nomEmetteur("Belaoud")
                                         .rib("RIB2")
                                         .motif("charge")
                                         .montant(BigDecimal.valueOf(500L))
                                         .build();
    }

    @Test
    public void allDeposits_should_succeed() throws Exception {
        mockMvc.perform(get(ALL_DEPOSIT_PATH).accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    public void effectuerDeposit_should_succeed() throws Exception {
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isCreated());
    }

    @Test
    public void effectuerDeposit_with_compte_beneficiaire_non_existant_should_fail() throws Exception {
        depositDto.setRib("xxxRandomxxx");
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isNotFound(),
                       result -> assertTrue(result.getResolvedException() instanceof CompteNonExistantException),
                       result -> assertEquals(COMPTE_NON_EXISTANT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerDeposit_with_min_amount_constraint_violation_should_fail() throws Exception {
        depositDto.setMontant(BigDecimal.ONE);
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MIN_AMOUNT_CONSTRAINT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerDeposit_with_max_amount_constraint_violation_should_fail() throws Exception {
        depositDto.setMontant(new BigDecimal(10001));
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MAX_AMOUNT_CONSTRAINT, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerdeposit_with_montant_null_should_fail() throws Exception {
        depositDto.setMontant(null);
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(COMPTE_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerdeposit_with_montant_zero_should_fail() throws Exception {
        depositDto.setMontant(BigDecimal.ZERO);
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(COMPTE_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerdeposit_with_null_motif_should_fail() throws Exception {
        depositDto.setMotif(null);
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MOTIF_VIDE, result.getResponse().getContentAsString())
               );
    }

    @Test
    public void effectuerdeposit_with_blank_motif_should_fail() throws Exception {
        depositDto.setMotif("       ");
        mockMvc.perform(post(EFFECTUER_DEPOSIT_PATH).content(asJsonString(depositDto))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isUnauthorized(),
                       result -> assertTrue(result.getResolvedException() instanceof TransactionException),
                       result -> assertEquals(MOTIF_VIDE, result.getResponse().getContentAsString())
               );
    }

    @AfterEach
    public void teardown() {
        depositDto = null;
    }

}
