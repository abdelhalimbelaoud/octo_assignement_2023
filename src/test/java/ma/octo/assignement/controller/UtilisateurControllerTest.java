package ma.octo.assignement.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exception.UtilisateurNonExistantException;
import ma.octo.assignement.service.impl.UtilisateurServiceImpl;
import ma.octo.assignement.util.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static ma.octo.assignement.constants.Constants.UTILISATEUR_NON_EXISTANT;
import static ma.octo.assignement.util.Util.asJavaObject;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UtilisateurControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    UtilisateurServiceImpl utilisateurService;

    final String ALL_UTILISATEURS_PATH = "/api/utilisateur/tousLesUtilisateurs";
    final String UTILISATEUR_BY_ID_PATH = "/api/utilisateur/";

    UtilisateurDto utilisateurDto;

    @BeforeEach
    public void init() {
        utilisateurDto = UtilisateurDto.builder().nomUtilisateur("user1")
                                                 .sexe("Male")
                                                 .nom("last1")
                                                 .prenom("first1")
                                                 .build();
    }

    @Test
    public void allUtilisateur_should_succeed() throws Exception {
        mockMvc.perform(get(ALL_UTILISATEURS_PATH).accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    public void getUtilisateur_should_succeed() throws Exception {
        given(utilisateurService.getUtilisateurById(1L)).willReturn(utilisateurDto);

        mockMvc.perform(get(UTILISATEUR_BY_ID_PATH + "1").accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                       status().isOk(),
                       result -> assertEquals(utilisateurDto, asJavaObject(result.getResponse().getContentAsString(),
                                                                           UtilisateurDto.class))
               );
    }

    @Test
    public void getUtilisateur_with_id_non_existant_should_fail() throws Exception {
        mockMvc.perform(get(UTILISATEUR_BY_ID_PATH + "4").accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpectAll(
                        status().isNotFound(),
                        result -> assertTrue(result.getResolvedException() instanceof UtilisateurNonExistantException),
                        result -> assertEquals(UTILISATEUR_NON_EXISTANT, result.getResponse().getContentAsString())
                );
    }
}
