package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

public class CompteMapper {

    public static CompteDto map(Compte compte) {
        return CompteDto.builder().nrCompte(compte.getNrCompte())
                                  .rib(compte.getRib())
                                  .solde(compte.getSolde())
                                  .build();
    }

}
