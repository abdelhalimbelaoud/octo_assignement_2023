package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.transaction.TransactionTransfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {

    public static TransferDto map(TransactionTransfer transfer) {
        return TransferDto.builder().nrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte())
                                    .nrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte())
                                    .date(transfer.getDateExecution())
                                    .montant(transfer.getMontant())
                                    .motif(transfer.getMotif())
                                    .build();
    }

}
