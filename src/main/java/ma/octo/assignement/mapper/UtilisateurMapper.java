package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

public class UtilisateurMapper {

    public static UtilisateurDto map(Utilisateur utilisateur) {
        return UtilisateurDto.builder().nomUtilisateur(utilisateur.getNomUtilisateur())
                                       .nom(utilisateur.getNom())
                                       .prenom(utilisateur.getPrenom())
                                       .sexe(utilisateur.getSexe())
                                       .dateNaissance(utilisateur.getDateNaissance())
                                       .build();
    }

}
