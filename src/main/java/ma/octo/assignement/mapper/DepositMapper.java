package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.transaction.TransactionDeposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {

    public static DepositDto map(TransactionDeposit deposit) {
        return DepositDto.builder().rib(deposit.getRib())
                                   .nomEmetteur(deposit.getNomEmetteur())
                                   .montant(deposit.getMontant())
                                   .motif(deposit.getMotif())
                                   .build();
    }

}
