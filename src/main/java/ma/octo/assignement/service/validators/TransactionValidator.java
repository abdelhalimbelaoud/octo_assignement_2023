package ma.octo.assignement.service.validators;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.utils.Util;

import java.math.BigDecimal;

import static ma.octo.assignement.constants.Constants.*;

@Slf4j
public class TransactionValidator {

    public static void validateTransaction(BigDecimal montant, String motif)
            throws TransactionException {
        if (montant == null || Util.isZero(montant)) {
            log.error(COMPTE_VIDE);
            throw new TransactionException(COMPTE_VIDE);
        } else if (Util.isLesser(montant, MIN_TRANSACTION)) {
            log.error(MIN_AMOUNT_CONSTRAINT);
            throw new TransactionException(MIN_AMOUNT_CONSTRAINT);
        } else if (Util.isGreater(montant, MAX_TRANSACTION)) {
            log.error(MAX_AMOUNT_CONSTRAINT);
            throw new TransactionException(MAX_AMOUNT_CONSTRAINT);
        }

        if (motif == null || motif.isBlank()) {
            log.error(MOTIF_VIDE);
            throw new TransactionException(MOTIF_VIDE);
        }
    }

    public static void validateDepositParties(Compte compteBeneficiaire)
            throws CompteNonExistantException {
        if (compteBeneficiaire == null) {
            log.error(COMPTE_NON_EXISTANT);
            throw new CompteNonExistantException(COMPTE_NON_EXISTANT);
        }
    }

    public static void validateTransferParties(Compte compteEmetteur, Compte compteBeneficiaire)
            throws CompteNonExistantException {
        if (compteEmetteur == null || compteBeneficiaire == null) {
            log.error(COMPTE_NON_EXISTANT);
            throw new CompteNonExistantException(COMPTE_NON_EXISTANT);
        }
    }

    public static void validateTransferSolde(BigDecimal solde, BigDecimal montant)
            throws SoldeDisponibleInsuffisantException {
        if (Util.isLesser(solde, montant)) {
            log.error(SOLDE_INSUFFISANT);
            throw new SoldeDisponibleInsuffisantException(SOLDE_INSUFFISANT);
        }
    }

}
