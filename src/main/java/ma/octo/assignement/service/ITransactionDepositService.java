package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionDepositException;
import ma.octo.assignement.exception.transaction.TransactionException;

import java.util.List;

public interface ITransactionDepositService {

    void deposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;

    List<DepositDto> getAllDeposits();

}
