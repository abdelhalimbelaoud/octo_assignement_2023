package ma.octo.assignement.service;

import java.math.BigDecimal;

public interface IAuditService {

    void transfer(String nrCompteEmetteur, String nrCompteBeneficiaire, BigDecimal montant);

    void deposit(String rib, BigDecimal montant);

}
