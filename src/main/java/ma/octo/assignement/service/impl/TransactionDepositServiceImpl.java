package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.transaction.TransactionDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionDepositRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ITransactionDepositService;
import ma.octo.assignement.service.validators.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class TransactionDepositServiceImpl implements ITransactionDepositService {

    @Autowired
    private TransactionDepositRepository depositRepository;

    @Autowired
    private IAuditService auditService;

    @Autowired
    private CompteRepository compteRepository;

    @Override
    public void deposit(DepositDto depositDto) throws TransactionException, CompteNonExistantException {
        Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRib());

        TransactionValidator.validateDepositParties(compteBeneficiaire);
        TransactionValidator.validateTransaction(depositDto.getMontant(), depositDto.getMotif());

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));

        compteRepository.save(compteBeneficiaire);
        depositRepository.save(TransactionDeposit.builder().dateExecution(new Date())
                                                           .nomEmetteur(depositDto.getNomEmetteur())
                                                           .montant(depositDto.getMontant())
                                                           .rib(depositDto.getRib())
                                                           .compteBeneficiaire(compteBeneficiaire)
                                                           .motif(depositDto.getMotif())
                                                           .build());

        auditService.deposit(depositDto.getRib(), depositDto.getMontant());
    }

    @Override
    public List<DepositDto> getAllDeposits() {
        return depositRepository.findAll().stream()
                                          .map(DepositMapper::map)
                                          .collect(Collectors.toList());
    }

}
