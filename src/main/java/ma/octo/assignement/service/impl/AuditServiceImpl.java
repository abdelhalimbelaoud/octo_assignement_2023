package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.audit.AuditDeposit;
import ma.octo.assignement.domain.audit.AuditTransfer;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.IAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ma.octo.assignement.constants.Constants.*;

import java.math.BigDecimal;

@Service
@Transactional
@Slf4j
public class AuditServiceImpl implements IAuditService {

    @Autowired
    AuditRepository auditRepository;

    @Override
    public void transfer(String nrCompteEmetteur, String nrCompteBeneficiaire, BigDecimal montant) {
        log.info(AUDIT_TRANSFER);
        String message = "Montant de " + montant + " a été transferer depuis le compte : " + nrCompteEmetteur +
                         " vers le compte " + nrCompteBeneficiaire;
        AuditTransfer audit = new AuditTransfer();
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    @Override
    public void deposit(String rib, BigDecimal montant) {
        log.info(AUDIT_DEPOSIT);
        String message = "Montant de " + montant + " a été déposé sur le compte avec le rib : " + rib;
        AuditDeposit audit = new AuditDeposit();
        audit.setMessage(message);
        auditRepository.save(audit);
    }

}
