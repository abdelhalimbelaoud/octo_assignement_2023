package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompteServiceImpl implements ICompteService {

    @Autowired
    private CompteRepository compteRepository;

    @Override
    public List<CompteDto> getAll() {
        return compteRepository.findAll().stream()
                                         .map(CompteMapper::map)
                                         .collect(Collectors.toList());
    }

}
