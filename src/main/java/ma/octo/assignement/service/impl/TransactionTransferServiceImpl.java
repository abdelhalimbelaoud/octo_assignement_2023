package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.transaction.TransactionTransfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionTransferRepository;
import ma.octo.assignement.service.ITransactionTransferService;
import ma.octo.assignement.service.validators.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionTransferServiceImpl implements ITransactionTransferService {

    @Autowired
    CompteRepository compteRepository;

    @Autowired
    TransactionTransferRepository transferRepository;

    @Autowired
    AuditServiceImpl auditService;

    @Override
    public void transfer(TransferDto transferDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        TransactionValidator.validateTransferParties(compteEmetteur, compteBeneficiaire);
        TransactionValidator.validateTransaction(transferDto.getMontant(), transferDto.getMotif());
        TransactionValidator.validateTransferSolde(compteEmetteur.getSolde(), transferDto.getMontant());

        BigDecimal eSoldeAfterTransfer = compteEmetteur.getSolde().subtract(transferDto.getMontant());
        compteEmetteur.setSolde(eSoldeAfterTransfer);
        compteRepository.save(compteEmetteur);

        BigDecimal bSoldeAfterTransfer = compteBeneficiaire.getSolde().add(transferDto.getMontant());
        compteBeneficiaire.setSolde(bSoldeAfterTransfer);
        compteRepository.save(compteBeneficiaire);

        transferRepository.save(TransactionTransfer.builder().dateExecution(new Date())
                                                             .compteBeneficiaire(compteBeneficiaire)
                                                             .compteEmetteur(compteEmetteur)
                                                             .montant(transferDto.getMontant())
                                                             .motif(transferDto.getMotif())
                                                             .build());

        auditService.transfer(
                transferDto.getNrCompteEmetteur(),
                transferDto.getNrCompteBeneficiaire(),
                transferDto.getMontant()
        );
    }

    @Override
    public List<TransferDto> getAllTransferts() {
        return transferRepository.findAll().stream()
                                           .map(TransferMapper::map)
                                           .collect(Collectors.toList());
    }

}
