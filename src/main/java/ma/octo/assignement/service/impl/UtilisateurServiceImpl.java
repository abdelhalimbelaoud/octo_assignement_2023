package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exception.UtilisateurNonExistantException;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ma.octo.assignement.constants.Constants.UTILISATEUR_NON_EXISTANT;

@Service
@Transactional
@Slf4j
public class UtilisateurServiceImpl implements IUtilisateurService {

    @Autowired
    UtilisateurRepository utilisateurRepository;

    public UtilisateurDto getUtilisateurById(Long id)
            throws UtilisateurNonExistantException {
        Optional<Utilisateur> utilisateur = utilisateurRepository.findById(id);

        return utilisateur.map(UtilisateurMapper::map)
                          .orElseThrow(() -> {
                              log.info(UTILISATEUR_NON_EXISTANT);
                              return new UtilisateurNonExistantException(UTILISATEUR_NON_EXISTANT);
                          });
    }

    @Override
    public List<UtilisateurDto> getAll() {
        return utilisateurRepository.findAll().stream()
                                              .map(UtilisateurMapper::map)
                                              .collect(Collectors.toList());
    }

}
