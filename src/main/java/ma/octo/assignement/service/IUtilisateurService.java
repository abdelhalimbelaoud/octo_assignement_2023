package ma.octo.assignement.service;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exception.UtilisateurNonExistantException;

import java.util.List;

public interface IUtilisateurService {

    UtilisateurDto getUtilisateurById(Long id) throws UtilisateurNonExistantException;
    List<UtilisateurDto> getAll();

}
