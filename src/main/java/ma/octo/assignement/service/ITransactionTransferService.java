package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.exception.transaction.TransactionTransferException;

import java.util.List;

public interface ITransactionTransferService {

    void transfer(TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;

    List<TransferDto> getAllTransferts();

}
