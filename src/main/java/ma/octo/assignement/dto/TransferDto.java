package ma.octo.assignement.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferDto {

    @EqualsAndHashCode.Include
    private String nrCompteEmetteur;

    @EqualsAndHashCode.Include
    private String nrCompteBeneficiaire;

    private String motif;

    private BigDecimal montant;

    private Date date;

}
