package ma.octo.assignement.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DepositDto {

    private String nomEmetteur;

    @EqualsAndHashCode.Include
    private String rib;

    private String motif;

    private BigDecimal montant;

}
