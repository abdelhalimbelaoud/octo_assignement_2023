package ma.octo.assignement.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class UtilisateurDto {

    @EqualsAndHashCode.Include
    private String nomUtilisateur;

    private String sexe;

    private String nom;

    private String prenom;

    private Date dateNaissance;

}
