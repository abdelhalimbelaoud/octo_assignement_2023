package ma.octo.assignement.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class CompteDto {

    private String nrCompte;

    @EqualsAndHashCode.Include
    private String rib;

    private BigDecimal solde;

}
