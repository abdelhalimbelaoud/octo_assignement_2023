package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import static ma.octo.assignement.constants.Constants.*;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/api/compte")
@Slf4j
public class CompteController {

    @Autowired
    private ICompteService compteService;

    @GetMapping("tousLesComptes")
    List<CompteDto> allComptes() {
        log.info(LISTE_DES_COMPTES);
        return compteService.getAll();
    }

}
