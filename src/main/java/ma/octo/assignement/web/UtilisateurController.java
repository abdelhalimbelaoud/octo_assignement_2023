package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import static ma.octo.assignement.constants.Constants.*;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exception.UtilisateurNonExistantException;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/utilisateur")
@Slf4j
public class UtilisateurController {

    @Autowired
    private IUtilisateurService utilisateurService;

    @GetMapping("/{id}")
    public UtilisateurDto getUtilisateur(@PathVariable("id")Long id)
            throws UtilisateurNonExistantException {
        return utilisateurService.getUtilisateurById(id);
    }

    @GetMapping("tousLesUtilisateurs")
    public List<UtilisateurDto> listerUtilisateurs() {
        log.info(LISTE_DES_UTILISATEURS);
        return utilisateurService.getAll();
    }

}
