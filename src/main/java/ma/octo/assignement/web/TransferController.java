package ma.octo.assignement.web;


import lombok.extern.slf4j.Slf4j;
import static ma.octo.assignement.constants.Constants.*;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;

import ma.octo.assignement.service.ITransactionTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transaction/transfert")
@Slf4j
class TransferController {

    @Autowired
    private ITransactionTransferService transferService;

    @GetMapping("/tousLesTransferts")
    List<TransferDto> allTranferts() {
        log.info(LISTE_DES_TRANSFERTS);
        return transferService.getAllTransferts();
    }

    @PostMapping("/effectuerTransfert")
    @ResponseStatus(HttpStatus.CREATED)
    public void effectuerTransfert(@RequestBody TransferDto transferDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        transferService.transfer(transferDto);
    }

}
