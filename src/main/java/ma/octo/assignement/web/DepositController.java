package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exception.CompteNonExistantException;
import ma.octo.assignement.exception.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exception.transaction.TransactionException;
import ma.octo.assignement.service.ITransactionDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ma.octo.assignement.constants.Constants.*;

@RestController
@RequestMapping("/api/transaction/deposit")
@Slf4j
public class DepositController {

    @Autowired
    private ITransactionDepositService depositService;

    @GetMapping("/tousLesDeposits")
    List<DepositDto> allDeposits() {
        log.info(LISTE_DES_TRANSFERTS);
        return depositService.getAllDeposits();
    }

    @PostMapping("/effectuerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void effectuerDeposit(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        depositService.deposit(depositDto);
    }

}
