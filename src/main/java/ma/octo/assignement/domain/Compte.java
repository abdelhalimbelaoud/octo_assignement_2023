package ma.octo.assignement.domain;

import lombok.*;
import ma.octo.assignement.domain.transaction.TransactionDeposit;
import ma.octo.assignement.domain.transaction.TransactionTransfer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "compte")
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 16, unique = true)
    private String nrCompte;

    private String rib;

    @Column(precision = 16, scale = 2)
    private BigDecimal solde;

    @ManyToOne()
    @JoinColumn(name = "id_utilisateur")
    private Utilisateur utilisateur;

    @OneToMany(mappedBy = "compteEmetteur")
    @ToString.Exclude
    Set<TransactionTransfer> transfersEmetteur;

    @OneToMany(mappedBy = "compteBeneficiaire")
    @ToString.Exclude
    Set<TransactionTransfer> transfersBeneficiaire;

    @OneToMany(mappedBy = "compteBeneficiaire")
    @ToString.Exclude
    Set<TransactionDeposit> depositsBeneficiaire;
}
