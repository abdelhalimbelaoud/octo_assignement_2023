package ma.octo.assignement.domain.audit;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "audit")
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
abstract public class Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 100)
    private String message;
}