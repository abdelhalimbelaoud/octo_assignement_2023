package ma.octo.assignement.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "utilisateur")
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 10, nullable = false, unique = true)
    private String nomUtilisateur;

    @Column(length = 10, nullable = false)
    private String sexe;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(length = 60, nullable = false)
    private String prenom;

    @Temporal(TemporalType.DATE)
    private Date dateNaissance;

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Utilisateur that = (Utilisateur) o;
      return id.equals(that.id);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id);
    }
}
