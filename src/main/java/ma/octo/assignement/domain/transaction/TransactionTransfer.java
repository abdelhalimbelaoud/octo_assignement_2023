package ma.octo.assignement.domain.transaction;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ma.octo.assignement.domain.Compte;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
public class TransactionTransfer extends Transaction {
    @ManyToOne
    Compte compteEmetteur;
}
