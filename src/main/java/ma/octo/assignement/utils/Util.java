package ma.octo.assignement.utils;

import java.math.BigDecimal;

public class Util {

    public static boolean isGreater(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) > 0;
    }

    public static boolean isLesser(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 0;
    }

    public static boolean isZero(BigDecimal a) {
        return a.equals(BigDecimal.ZERO);
    }

}
