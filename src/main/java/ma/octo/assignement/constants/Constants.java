package ma.octo.assignement.constants;

import java.math.BigDecimal;

public class Constants {

    public static final BigDecimal MIN_TRANSACTION = BigDecimal.TEN;
    public static final BigDecimal MAX_TRANSACTION = new BigDecimal(10000);

    public static final String UTILISATEUR_NON_EXISTANT = "Utilisateur non existant";

    public static final String COMPTE_NON_EXISTANT = "Compte non existant";
    public static final String COMPTE_VIDE = "Compte est vide";

    public static final String SOLDE_INSUFFISANT = "Solde est insuffisant";

    public static final String MIN_AMOUNT_CONSTRAINT = "Minimum montant (10) n'etait pas atteint";
    public static final String MAX_AMOUNT_CONSTRAINT = "Maximum montant (10000) etait depasse";

    public static final String MOTIF_VIDE = "Motif est vide";

    public static final String AUDIT_DEPOSIT = "Audit de type deposit";
    public static final String AUDIT_TRANSFER = "Audit de type transfer";

    public static final String LISTE_DES_COMPTES = "Tous les comptes étaient lister";
    public static final String LISTE_DES_UTILISATEURS = "Tous les utilisateurs ont étaient lister";
    public static final String LISTE_DES_TRANSFERTS = "Tous les utilisateurs ont étaient lister";

}
