package ma.octo.assignement.exception.transaction;

public class TransactionTransferException extends TransactionException {

    private static final long serialVersionUID = 1L;

    public TransactionTransferException(){};

    public TransactionTransferException(String message) {
        super(message);
    }

}
