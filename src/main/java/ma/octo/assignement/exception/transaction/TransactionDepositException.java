package ma.octo.assignement.exception.transaction;

public class TransactionDepositException extends TransactionException {

    private static final long serialVersionUID = 1L;

    public TransactionDepositException(){};

    public TransactionDepositException(String message) {
        super(message);
    }

}
