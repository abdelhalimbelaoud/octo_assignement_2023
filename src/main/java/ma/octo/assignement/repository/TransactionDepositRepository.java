package ma.octo.assignement.repository;

import ma.octo.assignement.domain.transaction.TransactionDeposit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionDepositRepository extends JpaRepository<TransactionDeposit, Long> {
}
