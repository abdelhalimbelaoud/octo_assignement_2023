package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.transaction.TransactionTransfer;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionTransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private TransactionTransferRepository transferRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setNomUtilisateur("user1");
		utilisateur1.setNom("last1");
		utilisateur1.setPrenom("first1");
		utilisateur1.setSexe("Male");

		utilisateurRepository.save(utilisateur1);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setNomUtilisateur("user2");
		utilisateur2.setNom("last2");
		utilisateur2.setPrenom("first2");
		utilisateur2.setSexe("Female");

		utilisateurRepository.save(utilisateur2);

		Compte compte1 = new Compte();
		compte1.setNrCompte("000001");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(2000L));
		compte1.setUtilisateur(utilisateur1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("000002");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(1400L));
		compte2.setUtilisateur(utilisateur2);

		compteRepository.save(compte2);

		TransactionTransfer v = new TransactionTransfer();
		v.setMontant(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotif("Assignment 2021");

		transferRepository.save(v);
	}
}
